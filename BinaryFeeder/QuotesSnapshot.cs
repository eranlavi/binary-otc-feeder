﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Xml;

namespace BinaryFeeder
{
    public class QuotesSnapshot
    {
        public class QuoteSnap
        {
            public string Symbol;
            public double Bid;
            public double Ask;
        }

        Logger logger;

        public QuotesSnapshot(Logger logger)
        {
            this.logger = logger;
        }

        public XmlDocument GetQuotesSnapshotFromYahoo()
        {
            XmlDocument xd = GetQuotesFromYahoo();
            if (xd == null)
            {
                System.Threading.Thread.Sleep(1000);
                xd = GetQuotesFromYahoo();
            }

            return xd;
        }

        private XmlDocument GetQuotesFromYahoo()
        {
            XmlDocument xd = null;

            try
            {
                string[] Symbols = ConfigurationManager.AppSettings["SymbolsSnapshot"].Split(';');

                string temp = string.Empty;
                for (int i = 0; i < Symbols.Length; i++)
                {
                    temp += "\"" + Symbols[i] + "\",";
                }

                if (temp.Length > 0)
                    temp = temp.Remove(temp.Length - 1);

                string Link = string.Format(ConfigurationManager.AppSettings["YahooLink"], temp).Replace(" ", "%20");
                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(Link);
                myReq.Timeout = 10000;
                myReq.ReadWriteTimeout = 10000;
                HttpWebResponse myWebResponse = (HttpWebResponse)myReq.GetResponse();                
                Stream dataStream = myWebResponse.GetResponseStream();

                using (StreamReader reader = new StreamReader(dataStream))
                {
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();
                    myWebResponse.Close();

                    xd = new XmlDocument();
                    xd.LoadXml(responseFromServer);
                }
            }
            catch (Exception ex)
            {
                logger.Log("GetQuotesFromYahoo exception: " + ex.Message);
                xd = null;

            }
            return xd;
        }

        public Dictionary<string, QuoteSnap> GetQuotesSnapshotFromMT4()
        {
            Dictionary<string, QuoteSnap> ld = new Dictionary<string, QuoteSnap>();

            string snapshot = GetQuotesFromMT4();
            if(string.IsNullOrEmpty(snapshot))
                snapshot = GetQuotesFromMT4();

            if (string.IsNullOrEmpty(snapshot))
                return ld;
            
            string[] stringSeparators = new string[] {"\r\n"};
            string[] result = snapshot.Split('\n');

            int counter = result.Length - 1;
            for (int i = 0; i < counter; i++)
            {
                string[] t = result[i].Split(' ');
                if (t.Length <= 2)
                    break;
                QuoteSnap qs = new QuoteSnap();
                qs.Symbol = t[1];
                qs.Bid = Convert.ToDouble(t[2]);
                qs.Ask = Convert.ToDouble(t[3]);
                ld.Add(qs.Symbol, qs);
            }

            return ld;
        }

        private string GetQuotesFromMT4()
        {
            string[] Symbols = ConfigurationManager.AppSettings["SymbolsSnapshot"].Split(';');

            string temp = string.Empty;
            for (int i = 0; i < Symbols.Length; i++)
            {
                temp += Symbols[i] + ",";
            }

            if (temp.Length > 0)
                temp = temp.Remove(temp.Length - 1);

            string Request = "WQUOTES-" + temp + "\nQUIT\n";

            System.Net.Sockets.TcpClient clientSocket = null;
            try
            {
                clientSocket = new System.Net.Sockets.TcpClient();
                clientSocket.Connect(ConfigurationManager.AppSettings["MT4IP"], Convert.ToInt32(ConfigurationManager.AppSettings["MT4Port"]));

                using (NetworkStream serverStream = clientSocket.GetStream())
                {
                    byte[] outStream = System.Text.Encoding.ASCII.GetBytes(Request);
                    serverStream.Write(outStream, 0, outStream.Length);
                    serverStream.Flush();

                    byte[] inStream = new byte[65536];
                    serverStream.Read(inStream, 0, (int)clientSocket.ReceiveBufferSize);
                    return System.Text.Encoding.ASCII.GetString(inStream);
                }
                               
            }
            catch (Exception ex)
            {
                logger.Log("GetQuotesFromMT4 exception: " + ex.Message);
                return string.Empty;
            }
            finally
            {
                if(clientSocket != null)
                    clientSocket.Close();
            }
        }

    }
}
