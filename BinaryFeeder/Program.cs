﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;
using System.Xml;
using System.Threading;
using System.Reflection;

namespace BinaryFeeder
{
    class Program
    {
        static Logger logger;
        static void Main(string[] args)
        {
            /*XmlDocument xDoc = new XmlDocument();
            xDoc.Load(@"C:\Temp\hello\Streams.xml");

            XmlNodeList nodes = xDoc.SelectNodes("//Streams/Stream");

            int ii = -1;
            foreach (XmlNode node in nodes)
            {
                ii++;
                XmlNode xn = nodes[ii];
                if (xn.Attributes["Symbol"].InnerText == "CONSTSYM" || xn.Attributes["Symbol"].InnerText == "OUNCESINGRAMS")
                    continue;
                int x = Convert.ToInt32(xn.Attributes["AntiSpikePoints"].InnerText);
                x = x * 2;
                xn.Attributes["OutputAntiSpikePoints"].InnerText = x.ToString();
                xn.Attributes["OutputFilter"].InnerText = "2";
            }

            xDoc.Save(@"C:\Temp\hello\Streams.xml");*/

            XmlDocument xd = null;
            Dictionary<string, QuotesSnapshot.QuoteSnap> ld = null;
            bool UseSymbolsStartMidFromConfig = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSymbolsStartMidFromConfig"]);
            bool UseYahoo = false;

            String assemblyLocationFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (String.Compare(Environment.CurrentDirectory, assemblyLocationFolder, StringComparison.OrdinalIgnoreCase) != 0)
            {
                Environment.CurrentDirectory = assemblyLocationFolder;
            }

            logger = new Logger();
            logger.Log("Starting Binary Feeder");

            UnifeedServer _UnifeedServer = new UnifeedServer();
            _UnifeedServer.Start();
            _UnifeedServer.ServerNotification += _UnifeedServer_ServerNotification;
            
            if (!UseSymbolsStartMidFromConfig)
            {
                QuotesSnapshot qs = new QuotesSnapshot(logger);
                //first try to take snapshot from meta. if failed try from yahoo.

                ld = qs.GetQuotesSnapshotFromMT4();
                if (ld.Count == 0)
                {
                    UseYahoo = true;
                    xd = qs.GetQuotesSnapshotFromYahoo();
                    if (xd == null)
                    {
                        //all snapshot attempts failed - notify by mail
                        logger.Log("Getting quotes snapshot failed - Feeder cannot start.");
                        logger.Log("Application self terminate...");
                        return;
                    }
                }                
            }

            if(!UseYahoo)
                logger.Log("Using MT4 snapshot");
            else
                logger.Log("Using YAHOO snapshot");

            string[] Symbols = ConfigurationManager.AppSettings["Symbols"].Split(';');
            for (int i = 0; i < Symbols.Length; i++)
            {
                SymbolFeeder sf = new SymbolFeeder(logger);

                //string[] Symbol = Symbols[i].Split('@');
                string[] Symbol = Symbols[i].Split('-');
                string FileName = ConfigurationManager.AppSettings["BaseFolder"] + @"\" + Symbol[0] + "\\script.txt";
                string OutputName = Symbol[1];

                int Digits = Convert.ToInt32(ConfigurationManager.AppSettings["Digits"].Split(';')[i]);

                double InitValue = 0.0;

                if (UseSymbolsStartMidFromConfig)
                    InitValue = Convert.ToDouble(ConfigurationManager.AppSettings["SymbolsStartMid"].Split(';')[i]);
                else
                {
                    if (UseYahoo)
                        InitValue = GetInitValueByYahoo(xd, Symbol[0], Digits);
                    else
                    {
                        InitValue = GetInitValueByMT4(ld, Symbol[0], Digits);
                    }

                    if (InitValue == 0.0)
                    {
                        logger.Log(OutputName + " start mid value: " + InitValue + " - Ignore");
                        continue;
                    }
                }

                int Spread = Convert.ToInt32(ConfigurationManager.AppSettings["SymbolsSpread"].Split(';')[i]);

                logger.Log(OutputName + " start mid value: " + InitValue);

                sf.Start(ConfigurationManager.AppSettings["BaseFolder"] + @"\" + Symbol[0] + @"\", _UnifeedServer, FileName, OutputName, InitValue, Spread, Digits);                
            }
            
            var autoResetEvent = new AutoResetEvent(false);
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                // cancel the cancellation to allow the program to shutdown cleanly
                eventArgs.Cancel = true;
                autoResetEvent.Set();
            };

            // main blocks here waiting for ctrl-C
            autoResetEvent.WaitOne();
        }

        static void _UnifeedServer_ServerNotification(string obj)
        {
            logger.Log(obj);
        }

        private static double GetInitValueByMT4(Dictionary<string, QuotesSnapshot.QuoteSnap> ld, string Symbol, int Digits)
        {
            try
            {
                if(!ld.ContainsKey(Symbol))
                    return 0.0;

                QuotesSnapshot.QuoteSnap qs = ld[Symbol];

                double Mid = (qs.Ask + qs.Bid) / 2;
                return Math.Floor(Mid * Math.Pow(10, Digits)) / Math.Pow(10, Digits);
            }
            catch (Exception ex)
            {
                logger.Log("GetInitValueByMT4 exception for " + Symbol + ": " + ex.Message);
                return 0.0;
            }
        }

        private static double GetInitValueByYahoo(XmlDocument xd, string Symbol, int Digits)
        {
            try
            {
                XmlNodeList nodes = xd.SelectNodes("//query/results/rate [@id='" + Symbol + "']/Ask");
                XmlNode node = nodes[0];
                double Ask = Convert.ToDouble(node.InnerText);
                nodes = xd.SelectNodes("//query/results/rate [@id='" + Symbol + "']/Bid");
                node = nodes[0];
                double Bid = Convert.ToDouble(node.InnerText);

                double Mid = (Ask + Bid) / 2;
                return Math.Floor(Mid * Math.Pow(10, Digits)) / Math.Pow(10, Digits);
            }
            catch (Exception ex)
            {
                logger.Log("GetInitValueByYahoo exception for " + Symbol + ": " + ex.Message);
                return 0.0;
            }
        }


    }
}
