﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace BinaryFeeder
{
    public class SymbolFeeder
    {
        Logger logger;
        public SymbolFeeder(Logger logger)
        {
            this.logger = logger;
        }

        private string ToFastDateTimeString()
        {
            DateTime Date = DateTime.Now;
            char[] chars = new char[23];
            chars[0] = Digit(Date.Day / 10);
            chars[1] = Digit(Date.Day % 10);
            chars[2] = '/';
            chars[3] = Digit(Date.Month / 10);
            chars[4] = Digit(Date.Month % 10);
            chars[5] = '/';
            chars[6] = Digit(Date.Year / 1000);
            chars[7] = Digit(Date.Year % 1000 / 100);
            chars[8] = Digit(Date.Year % 100 / 10);
            chars[9] = Digit(Date.Year % 10);
            chars[10] = '-';
            chars[11] = Digit(Date.Hour / 10);
            chars[12] = Digit(Date.Hour % 10);
            chars[13] = ':';
            chars[14] = Digit(Date.Minute / 10);
            chars[15] = Digit(Date.Minute % 10);
            chars[16] = ':';
            chars[17] = Digit(Date.Second / 10);
            chars[18] = Digit(Date.Second % 10);
            chars[19] = '.';
            chars[20] = Digit(Date.Millisecond / 100);
            chars[21] = Digit(Date.Millisecond % 100 / 10);
            chars[22] = Digit(Date.Millisecond % 10);

            return new string(chars);
        }
        private char Digit(int value) { return (char)(value + '0'); }

        public void Start(string BaseFolder, UnifeedServer _UnifeedServer, string FileName, string OutputName, double InitValue, int Spread, int Digits)
        {
            double bid = 0.0;
            double ask = 0.0;

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReadLastPriceFromFile"]))
            {               
                GetLastPrice(BaseFolder, out bid, out ask);
                
                logger.Log("Reading bid/ask from File for " + OutputName + ": " + bid + "/" + ask);
            }
            
            if (bid == 0.0 || ask == 0.0)
            {
                bid = InitValue - ((Spread / 2) / Math.Pow(10, Digits));
                ask = InitValue + ((Spread / 2) / Math.Pow(10, Digits));

                logger.Log("Building bid/ask for " + OutputName + " from init value (" + InitValue + "): " + bid + "/" + ask);
            }

            System.Threading.Tasks.Task.Factory.StartNew(() => StartFeed(BaseFolder, _UnifeedServer, FileName, OutputName, bid, ask, Digits));
        }

        private void StartFeed2(string BaseFolder, UnifeedServer _UnifeedServer, string FileName, string OutputName, double bid, double ask, int Digits)
        {
            int i = 0;
            string[] Lines = System.IO.File.ReadAllLines(FileName);

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReadLastPriceFromFile"]))
                i = GetLastLine(BaseFolder);

            if (i >= Lines.Length)
                i = 0;
         
            logger.Log("Start script for " + OutputName + ". Starting from line: " + i.ToString());
            i = 0;
            double xxx = 0.0;
            for (; i < Lines.Length; i++)
            {
                try
                {
                    //WriteLastLine(BaseFolder, i);
                    
                    Lines[i] = Lines[i].Replace("\t", " ").Replace(",", " ").Replace("\"", "");
                    
                    string[] data = Lines[i].Split(' ');
                    if (data.Length == 0)
                        continue;
                    
                    //System.Threading.Thread.Sleep(Convert.ToInt32(data[0]));
                    
                    double value = Convert.ToDouble(data[1].Trim());
                    if (value >= 1.0 || value <= -1.0)
                    {
                        logger.Log(OutputName + " - Invalid data on line " + i.ToString());
                        continue;
                    }

                    //bid = bid + value;
                    //ask = ask + value;

                    //string sBid = bid.ToString("F" + Digits);
                    //string sAsk = ask.ToString("F" + Digits);
                    xxx += value;
                    //_UnifeedServer.SendData(OutputName + " " + ToFastDateTimeString() + " " + sBid + " " + sAsk + "\r\n");

                    //WriteLastPrice(BaseFolder, sBid, sAsk);
                }
                catch (Exception ex)
                {
                    logger.Log("Excetion sending quote: " + ex.Message + " , Line: " + Lines[i]);
                }
            }

            logger.Log("Finished script for " + OutputName);
            WriteLastLine(BaseFolder, 0);
            //System.Threading.Tasks.Task.Factory.StartNew(() => StartFeed(BaseFolder, _UnifeedServer, FileName, OutputName, bid, ask, Digits));
            Console.WriteLine(OutputName + " " + xxx.ToString("F" + Digits));
            System.Diagnostics.Debug.WriteLine(OutputName + " " + xxx.ToString("F" + Digits));            
        }

        private void StartFeed_simulator(string BaseFolder, UnifeedServer _UnifeedServer, string FileName, string OutputName, double bid, double ask, int Digits)
        {
            int i = 0;
            string[] Lines = System.IO.File.ReadAllLines(FileName);

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReadLastPriceFromFile"]))
                i = GetLastLine(BaseFolder);

            if (i >= Lines.Length)
                i = 0;

            //logger.Log("Start script for " + OutputName + ". Starting from line: " + i.ToString());
            //i = 0;
            for (; i < Lines.Length; i++)
            {
                try
                {
                    WriteLastLine(BaseFolder, i);

                    Lines[i] = Lines[i].Replace("\t", " ").Replace(",", " ").Replace("\"", "");

                    string[] data = Lines[i].Split(' ');
                    if (data.Length == 0)
                        continue;

                    System.Threading.Thread.Sleep(Convert.ToInt32(data[0]));

                    double value = Convert.ToDouble(data[1].Trim());
                    if (value >= 1.0 || value <= -1.0)
                    {
                        logger.Log(OutputName + " - Invalid data on line " + i.ToString());
                        continue;
                    }

                    bid = bid + value;
                    ask = ask + value;

                    string sBid = bid.ToString("F" + Digits);
                    string sAsk = ask.ToString("F" + Digits);

                    int Sleep = Convert.ToInt32(ConfigurationManager.AppSettings["Sleep"]);
                    while (true)
                    {
                        _UnifeedServer.SendData(OutputName + " " + ToFastDateTimeString() + " " + sBid + " " + sAsk + "\r\n");
                        bid = bid + 0.01;
                        ask = ask + 0.01;
                        sBid = bid.ToString("F" + Digits);
                        sAsk = ask.ToString("F" + Digits);
                        System.Threading.Thread.Sleep(Sleep);
                    }

                    WriteLastPrice(BaseFolder, sBid, sAsk);
                }
                catch (Exception ex)
                {
                    logger.Log("Excetion sending quote: " + ex.Message + " , Line: " + Lines[i]);
                }
            }

            //logger.Log("Finished script for " + OutputName);
            WriteLastLine(BaseFolder, 0);
            System.Threading.Tasks.Task.Factory.StartNew(() => StartFeed(BaseFolder, _UnifeedServer, FileName, OutputName, bid, ask, Digits));
        }

        private void StartFeed(string BaseFolder, UnifeedServer _UnifeedServer, string FileName, string OutputName, double bid, double ask, int Digits)
        {
            int i = 0;
            string[] Lines = System.IO.File.ReadAllLines(FileName);

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ReadLastPriceFromFile"]))
                i = GetLastLine(BaseFolder);

            if (i >= Lines.Length)
                i = 0;

            //logger.Log("Start script for " + OutputName + ". Starting from line: " + i.ToString());
            //i = 0;
            for (; i < Lines.Length; i++)
            {
                try
                {
                    WriteLastLine(BaseFolder, i);

                    Lines[i] = Lines[i].Replace("\t", " ").Replace(",", " ").Replace("\"", "");

                    string[] data = Lines[i].Split(' ');
                    if (data.Length == 0)
                        continue;

                    System.Threading.Thread.Sleep(Convert.ToInt32(data[0]));

                    double value = Convert.ToDouble(data[1].Trim());
                    if (value >= 1.0 || value <= -1.0)
                    {
                        logger.Log(OutputName + " - Invalid data on line " + i.ToString());
                        continue;
                    }

                    bid = bid + value;
                    ask = ask + value;

                    string sBid = bid.ToString("F" + Digits);
                    string sAsk = ask.ToString("F" + Digits);

                    _UnifeedServer.SendData(OutputName + " " + ToFastDateTimeString() + " " + sBid + " " + sAsk + "\r\n");

                    WriteLastPrice(BaseFolder, sBid, sAsk);
                }
                catch (Exception ex)
                {
                    logger.Log("Excetion sending quote: " + ex.Message + " , Line: " + Lines[i]);
                }
            }

            //logger.Log("Finished script for " + OutputName);
            WriteLastLine(BaseFolder, 0);
            System.Threading.Tasks.Task.Factory.StartNew(() => StartFeed(BaseFolder, _UnifeedServer, FileName, OutputName, bid, ask, Digits));
        }

        private int GetLastLine(string BaseFolder)
        {
            string file = BaseFolder + "LastLine.txt";
            try
            {
                string text = System.IO.File.ReadAllText(file);
                return Convert.ToInt32(text);
            }
            catch (Exception ex)
            {
                logger.Log("GetLastLine for " + file + " exception: " + ex.Message);
            }

            return 0;
        }

        private void WriteLastLine(string BaseFolder, int Line)
        {
            //return;
            string file = BaseFolder + "LastLine.txt";
            try
            {
                System.IO.File.WriteAllText(file, Line.ToString());
            }
            catch (Exception ex)
            {
                logger.Log("WriteLastLine for " + file + " exception: " + ex.Message);
            }
        }

        private void GetLastPrice(string BaseFolder, out double bid, out double ask)
        {
            bid = 0.0;
            ask = 0.0;
            string file = BaseFolder + "LastPrice.txt";

            try
            {
                string text = System.IO.File.ReadAllText(file);
                string[] temp = text.Split(' ');
                bid = Convert.ToDouble(temp[0]);
                ask = Convert.ToDouble(temp[1]);
            }
            catch (Exception ex)
            {
                logger.Log("GetLastPrice for " + file + " exception: " + ex.Message);
            }            
        }

        private void WriteLastPrice(string BaseFolder, string bid, string ask)
        {
            //return;
            string file = BaseFolder + "LastPrice.txt";
            try
            {
                System.IO.File.WriteAllText(file, bid + " " + ask);
            }
            catch (Exception ex)
            {
                logger.Log("WriteLastPrice for " + file + " exception: " + ex.Message);
            }
        }


    }
}
