﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ScriptBuilder
{
    public class Logger
    {
        object obj;
        string BaseFolder;

        public Logger()
        {
            obj = new object();

            BaseFolder = System.Configuration.ConfigurationManager.AppSettings["LogFolder"];
            DirectoryInfo dir = new DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["LogFolder"] + "xxx.txt");
            if (!dir.Parent.Exists)
                dir.Parent.Create();
        }

        public void Log(string Data)
        {
            try
            {
                lock (obj)
                {
                    string logFile = BaseFolder + DateTime.Now.ToString("ddMMyyyy") + ".txt";
                    //if (!System.IO.File.Exists(logFile))
                    //    System.IO.File.Create(logFile);
                    System.IO.File.AppendAllText(logFile, string.Format("{0:dd/MM/yyyy HH:mm:ss:fff} ", DateTime.Now) + " " + Data + "\r\n");
                }
            }
            catch { }
        }
    }
}
