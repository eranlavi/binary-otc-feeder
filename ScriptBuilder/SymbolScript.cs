﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScriptBuilder
{
    public class SymbolScript
    {
        private class Datum
        {
            public double Mid = 0.0;
            public TimeSpan ts;
        }

        private class Script
        {
            public double PtsChanged;
            public double TimeChanged;
        }

        Logger logger;

        public SymbolScript(Logger logger)
        {
            this.logger = logger;
        }

        public void Run(string Folder, string SymbolName, string FileName, int SymbolDigit, string OutputFileName, double SymbolMultiplier, int SymbolDigitFinal)
        {
            try
            {
                System.IO.File.Delete(Folder + "Script.txt");
            }
            catch { }

            try
            {
                System.IO.File.Delete(Folder + "Script.csv");
            }
            catch { }

            System.IO.File.WriteAllText(Folder + "LastLine.txt", "0");

            double constant = Math.Pow(10, SymbolDigit);

            List<Datum> lDatum = new List<Datum>();

            string[] Lines = System.IO.File.ReadAllLines(FileName);
            for (int i = 1; i < Lines.Length; i++)
            {
                string[] data = Lines[i].Split(',');

                Datum d = new Datum();

                d.Mid = (Convert.ToDouble(data[1]) + Convert.ToDouble(data[2])) / 2;
                d.Mid = Math.Floor(d.Mid * constant) / constant;
                d.ts = TimeSpan.Parse(data[0].Split('-')[1]);

                lDatum.Add(d);
            }

            List<Script> lScript = new List<Script>();
            int Count = lDatum.Count - 1;
            for (int i = 0; i < Count; i++)
            {
                Script s = new Script();

                s.PtsChanged = (lDatum[i + 1].Mid - lDatum[i].Mid) * SymbolMultiplier;
                string temp = s.PtsChanged.ToString("F" + SymbolDigitFinal);
                s.PtsChanged = Convert.ToDouble(temp);
                //s.PtsChanged = Math.Floor(s.PtsChanged * constant) / constant;
                s.TimeChanged = (lDatum[i + 1].ts - lDatum[i].ts).TotalMilliseconds;

                lScript.Add(s);
            }
                        
            string format = "F" + SymbolDigitFinal;
            try
            {
                System.IO.File.Delete(OutputFileName);
            }
            catch { }

            //
            //double[] ttt = new double[lScript.Count];
            //double[] outttt = new double[lScript.Count];
            //for (int i = 0; i < lScript.Count; i++)
            //    ttt[i] = lScript[i].PtsChanged;
            //int begidx;
            //int nbelem;
            //int time = 2;
            //TicTacTec.TA.Library.Core.RetCode rc = TicTacTec.TA.Library.Core.(0, ttt.Length - 1, ttt, time, out begidx, out nbelem, outttt);
           
            //

            double FinalChange = 0.0;
            for (int i = 0; i < lScript.Count; i++)
            {
                if (lScript[i].TimeChanged < 0)
                    continue;
                FinalChange += lScript[i].PtsChanged;
                System.IO.File.AppendAllText(OutputFileName, lScript[i].TimeChanged + " " + lScript[i].PtsChanged.ToString(format) + "\r\n");
            }
            
            logger.Log(SymbolName + " - Num of changes (rate): " + lScript.Count.ToString());
            logger.Log(SymbolName + " - Final change: " + FinalChange.ToString(format));

            lScript.Sort(delegate(Script c1, Script c2) { return c1.PtsChanged.CompareTo(c2.PtsChanged); });
            logger.Log(SymbolName + " - Highest DOWN change: " + lScript[0].PtsChanged.ToString(format));
            logger.Log(SymbolName + " - Highest UP change: " + lScript[lScript.Count - 1].PtsChanged.ToString(format));
          
            Console.WriteLine("Finished building script for " + SymbolName);
            logger.Log("Finished building script for " + SymbolName);
        }

       
    }
}
