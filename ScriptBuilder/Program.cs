﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ScriptBuilder
{
    class Program
    {
        static Logger logger;
        
        static void Main(string[] args)
        {            
            String assemblyLocationFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (String.Compare(Environment.CurrentDirectory, assemblyLocationFolder, StringComparison.OrdinalIgnoreCase) != 0)
            {
                Environment.CurrentDirectory = assemblyLocationFolder;
            }

            logger = new Logger();
            logger.Log("Starting Script Builder");

            string[] Symbols = ConfigurationManager.AppSettings["Symbols"].Split(';');
            //for (int i = 0; i < 1; i++)
            for (int i = 0; i < Symbols.Length; i++)
            {
                SymbolScript ss = new SymbolScript(logger);
                string SymbolName = Symbols[i];
                string FileName = ConfigurationManager.AppSettings["BaseFolder"] + @"\" + Symbols[i] + @"\" + Symbols[i] + ".csv";
                string OutputFileName = ConfigurationManager.AppSettings["BaseFolder"] + @"\" + Symbols[i] + @"\" + ConfigurationManager.AppSettings["SymbolScriptFileName"] + ".txt";
                int SymbolDigit = Convert.ToInt32(ConfigurationManager.AppSettings["SymbolsDigits"].Split(';')[i]);
                double SymbolMultiplier = Convert.ToDouble(ConfigurationManager.AppSettings["SymbolsMultiplier"].Split(';')[i]);
                int SymbolDigitFinal = Convert.ToInt32(ConfigurationManager.AppSettings["SymbolsFinalDigits"].Split(';')[i]);
                string Folder = ConfigurationManager.AppSettings["BaseFolder"] + @"\" + Symbols[i] + @"\";
                System.Threading.Tasks.Task.Factory.StartNew(() => ss.Run(Folder, SymbolName, FileName, SymbolDigit, OutputFileName, SymbolMultiplier, SymbolDigitFinal));
            }
           
            Console.ReadKey();
        }
    }
}
